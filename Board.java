public class Board{
  private Die dice1;
  private Die dice2;
  private boolean[] closedTiles;
  public Board(){
    this.dice1 = new Die();
    this.dice2 = new Die();
    this.closedTiles = new boolean[12];
  }
  public String toString(){
    String tiles = " ";
    for (int i=0; i < this.closedTiles.length; i++){
      if (!closedTiles[i] ){
        int y= i+1;
        tiles = tiles + y +" ";
      }
      else{
        tiles = tiles + "X ";
      }
    }
   return tiles;
  }
  public boolean playATurn(){
    this.dice1.roll();
    this.dice2.roll();
    System.out.println("dice 1 " + dice1);
    System.out.println("dice 2 " + dice2);
    System.out.println("");
    int sumDice = dice1.getPips() + dice2.getPips();
    if (!closedTiles[sumDice-1]  ){
      closedTiles[sumDice-1] = true;
      System.out.println("Closing Tiles:" + sumDice);
      return false;
    }
      else{
        System.out.println("Tiles is already shut");
        return true;
      }
    
  }
}